/*
 * File: naxriscv.nix
 * Project: env
 * Created Date: Monday November 14th 2022
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Monday, 14th November 2022 3:45:58 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2022 INRIA
 */

let
    pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify                
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-22.05";                     
        rev = "ce6aa13369b667ac2542593170993504932eb836";                                           
    }) {};

    llvm-custom = (import ./llvm.nix);

in

with import <nixpkgs> { 
};

# Make a new "derivation" that represents our shell
stdenv.mkDerivation {
    name = "llvm";

    # The packages in the `buildInputs` list will be added to the PATH in our shell
    nativeBuildInputs = with pkgs; [
        ninja
        cmake
        python3
        git
    ];
}
