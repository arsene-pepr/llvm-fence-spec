with import <nixpkgs> {};

let
    pkgs = import (builtins.fetchGit {
        # Descriptive name to make the store path easier to identify                
        name = "pinned_nix_packages";                                                 
        url = "https://github.com/nixos/nixpkgs/";                       
        ref = "nixos-22.05";                     
        rev = "ce6aa13369b667ac2542593170993504932eb836";                                           
    }) {};

in


stdenv.mkDerivation rec {
  pname = "llvm-custom-riscv";
  version = "custom";

  src = fetchgit {
    url = "https://gitlab.inria.fr/arsene-pepr/llvm-fence-spec.git";
    rev = "23e41a13a79388054a5daf94e4a6ba1099fe6c29";
    hash = "sha256-5tJWfQZsJS1tSEwumaqAalDjqjCqgpCXP11meufFrv4=";
  };

  nativeBuildInputs = [ cmake ninja python3 git];

  BUILD_DIR = "./build";
  INSTALL_DIR = "./install";
  LLVM_DIR = "../llvm";
  ENABLE_PROJECTS = "clang";

  configureFlags = [
    "-G Ninja"
    "-DCMAKE_BUILD_TYPE=Release"
    "-DBUILD_SHARED_LIBS=True"
    "-DLLVM_USE_SPLIT_DWARF=True"
    "-DCMAKE_INSTALL_PREFIX=${INSTALL_DIR}"
    "-DLLVM_OPTIMIZED_TABLEGEN=True"
    "-DLLVM_BUILD_TESTS=False"
    "-DDEFAULT_SYSROOT=${INSTALL_DIR}/riscv64-spec-elf"
    "-DLLVM_DEFAULT_TARGET_TRIPLE=riscv64-spec-elf"
    "-DLLVM_TARGETS_TO_BUILD=RISCV"
    "-DLLVM_ENABLE_PROJECTS=${ENABLE_PROJECTS}"
  ];

  configurePhase = ''
    echo *********** ${BUILD_DIR}
    mkdir -p ${BUILD_DIR}
    cd ${BUILD_DIR}
    cmake ${LLVM_DIR} ${builtins.concatStringsSep " " configureFlags}
  '';

  buildPhase = ''
    cmake --build . -j 8
  '';

  installPhase = ''
    cmake --install .
    mkdir -p $out/bin
    cp -r ${INSTALL_DIR}/* $out
  '';

  # Meta information, etc.
}

