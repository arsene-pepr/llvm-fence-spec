
FENCE_TESTS = llvm/test/CodeGen/RISCV/speculative-execution-side-effect-suppression.ll llvm/test/CodeGen/RISCV/fence-spec-dummy.ll llvm/test/CodeGen/RISCV/speculative-load-hardening-moein.ll

OPT_TESTS = llvm/test/CodeGen/RISCV/O0-pipeline.ll llvm/test/CodeGen/RISCV/O3-pipeline.ll

DUMMY_TEST = llvm/test/CodeGen/RISCV/fence-spec-dummy.ll

FENCE_TEST = llvm/test/CodeGen/RISCV/speculative-execution-side-effect-suppression.ll

SLH_TEST = llvm/test/CodeGen/RISCV/speculative-load-hardening-moein.ll

test-riscv:
	_build/bin/llvm-lit -v llvm/test/CodeGen/RISCV

test-fence:
	_build/bin/llvm-lit -v $(FENCE_TESTS)

test-opt:
	_build/bin/llvm-lit -v $(OPT_TESTS)

asm-fence-no-end:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-disopt $(FENCE_TEST) -o mytests/test.S -stats

asm-fence:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-end-block -riscv-fence-spec-disopt $(FENCE_TEST) -o mytests/test.S -stats

asm-fence-ser-slh:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-slh -riscv-fence-spec-ser -riscv-fence-spec-omit-branch-fences $(FENCE_TEST) -o mytests/slh-fence.S -stats

asm-fence-slh:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-slh -riscv-fence-spec-omit-branch-fences $(FENCE_TEST) -o mytests/slh-fence.S -stats

asm-fence-nop:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-nop $(FENCE_TEST) -o mytests/nop.S -stats

asm-fence-opt:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-omit-branch-fences $(FENCE_TEST) -o mytests/test.opt.S -stats

asm-fence-all:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-all $(FENCE_TEST) -o mytests/test.S -stats

asm-fence-ser-all:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-ser -riscv-fence-spec-all $(FENCE_TEST) -o mytests/test.S -stats

asm-fence-ser-after:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-ser -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-after $(FENCE_TEST) -o mytests/test.S -stats

asm-fence-spec-after:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-after -O0 $(DUMMY_TEST) -o mytests/test.S -stats

asm-fence-spec-after-dis:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-fence-spec-enable -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-after -riscv-fence-spec-after-disable-x0 $(FENCE_TEST) -o mytests/test.S -stats

build:
	cmake --build _build/ -j 16 --target install

clean:
	cmake --build _build/ -j 16 --target clean

check-llvm:
	cmake --build _build/ -j 16 --target check-llvm

asm-slh-fence:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-speculative-load-hardening -riscv-slh-fence $(SLH_TEST) -o mytests/slh_fence_moein.S -stats

asm-slh:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-speculative-load-hardening $(SLH_TEST) -o mytests/slh_moein.S -stats

asm-fence-cond:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-speculative-load-hardening -riscv-slh-fence-cond $(SLH_TEST) -o mytests/slh_moein_fence_cond_branch.S -stats -debug

asm-fence-cond-ip:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-speculative-load-hardening -riscv-slh-fence-cond -riscv-slh-ip $(SLH_TEST) -o mytests/slh_moein_fence_cond_branch.S -stats -debug

asm-slh-ip:
	llc -mtriple=riscv64-unknown-linux-gnu -riscv-speculative-load-hardening -riscv-slh-ip $(SLH_TEST) -o mytests/slh_moein_fence_cond_branch.S -stats -debug -O0

# asm-slh-ip:
# 	llc -mtriple=riscv64 -riscv-speculative-load-hardening -riscv-slh-fence-cond -riscv-slh-ip $(SLH_TEST) -o mytests/slh_moein_fence_cond_branch_ip.S

# clang-asm-slh-ip:
# 	clang -mllvm --riscv-speculative-load-hardening -mllvm --riscv-slh-ip mytests/victim.c -S -emit-llvm -mllvm -stats -mllvm -debug -mabi=lp64d -march=rv64gc -o mytests/slh_moein_fence_dep_branch.S -mllvm -stats


asm:
	llc -mtriple=riscv64-unknown-linux-gnu $(SLH_TEST) -o mytests/slh.S -debug -stats

test-slh:
	_build/bin/llvm-lit -v $(SLH_TEST)

beebscO2:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O2 -mllvm --debug \
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening -mllvm --riscv-slh-fence-cond \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -o beebsc.o \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c

beebscO3:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O3 -mllvm --riscv-slh-ip\
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening -mllvm --riscv-slh-fence-cond \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -o beebsc.o \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c

asm-beebsc:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O2 -mllvm --debug \
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening -mllvm --riscv-slh-fence-cond \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -S -o beebsc.s \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c

beebscO0:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O0 \
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening -mllvm --riscv-slh-fence-cond -mllvm --riscv-slh-ip \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -S -o beebsc.s \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c

beebscO1:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O1 -mllvm --debug \
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening -mllvm --riscv-slh-fence-cond -mllvm --riscv-slh-ip \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -S -o beebsc.s \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c


beebsc:
	clang -g \
  -c -v -march=rv64gc -mabi=lp64d -ffunction-sections -fdata-sections \
  -fPIC --target=riscv64-spec-elf -O3 -mllvm --debug \
  --gcc-toolchain=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf \
  --sysroot=/nix/store/nwqnvdjwdwx1x0b0nmgz51ry5gwbkv45-riscv64-spec-elf/riscv64-spec-elf \
  -mspeculative-load-hardening \
  -I/home/trubiano/riscv/spectre/evals/embench/support \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/boards/naxverilator \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64/chips/nax \
  -I/home/trubiano/riscv/spectre/evals/embench/config/riscv64 \
  -DCPU_MHZ=1 -DWARMUP_HEAT=1 -o beebsc.o \
  /home/trubiano/riscv/spectre/evals/embench/support/beebsc.c

