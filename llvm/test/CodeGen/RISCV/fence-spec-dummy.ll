; RUN: llc -mtriple=riscv64 -riscv-fence-spec-enable -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-disopt -O0 %s -o - | FileCheck %s --check-prefix=FENCE-SPEC-O0

; RUN: llc -mtriple=riscv64 -riscv-fence-spec-omit-branch-fences -riscv-fence-spec-enable -O0 %s -o - | FileCheck %s --check-prefix=FENCE-OPT


@array1_size = dso_local global i32 16, align 4
@array1 = dso_local global <{ [16 x i8], [144 x i8] }> <{ [16 x i8] c"\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F\10", [144 x i8] zeroinitializer }>, align 1
@temp = dso_local global i8 0, align 1
@array2 = dso_local global [131072 x i8] zeroinitializer, align 1

define dso_local void @load_seq(i64 noundef %x) {
; FENCE-SPEC-O0-LABEL: load_seq:
; FENCE-SPEC-O0:       # %bb.0:
;
; FENCE-SPEC-O0-NEXT:	 	addi	sp, sp, -16
; FENCE-SPEC-O0-NEXT:    .cfi_def_cfa_offset 16
; FENCE-SPEC-O0-NEXT:    sd	a0, 8(sp)
; FENCE-SPEC-O0-NEXT:    fence.spec	sp, sp
; FENCE-SPEC-O0-NEXT:    ld	a1, 8(sp)
; FENCE-SPEC-O0-NEXT:    lui	a0, %hi(array1)
; FENCE-SPEC-O0-NEXT:    addi	a0, a0, %lo(array1)
; FENCE-SPEC-O0-NEXT:    add	a0, a0, a1
; FENCE-SPEC-O0-NEXT:    fence.spec	a0, a0
; FENCE-SPEC-O0-NEXT:    ld	a2, 0(a0)
; FENCE-SPEC-O0-NEXT:    lui	a1, %hi(temp)
; FENCE-SPEC-O0-NEXT:    sd	a2, %lo(temp)(a1)
; FENCE-SPEC-O0-NEXT:    fence.spec	a0, a0
; FENCE-SPEC-O0-NEXT:    ld	a2, 8(a0)
; FENCE-SPEC-O0-NEXT:    sd	a2, %lo(temp)(a1)
; FENCE-SPEC-O0-NEXT:    fence.spec	a0, a0
; FENCE-SPEC-O0-NEXT:    ld	a0, 16(a0)
; FENCE-SPEC-O0-NEXT:    sd	a0, %lo(temp)(a1)
; FENCE-SPEC-O0-NEXT:    addi	sp, sp, 16
; FENCE-SPEC-O0-NEXT:    ret

; FENCE-OPT-LABEL: load_seq:
; FENCE-OPT:       # %bb.0:
;
; FENCE-OPT-NEXT:	 	addi	sp, sp, -16
; FENCE-OPT-NEXT:    .cfi_def_cfa_offset 16
; FENCE-OPT-NEXT:    sd	a0, 8(sp)
; FENCE-OPT-NEXT:    fence.spec	sp, sp
; FENCE-OPT-NEXT:    ld	a1, 8(sp)
; FENCE-OPT-NEXT:    lui	a0, %hi(array1)
; FENCE-OPT-NEXT:    addi	a0, a0, %lo(array1)
; FENCE-OPT-NEXT:    add	a0, a0, a1
; FENCE-OPT-NEXT:    fence.spec	a0, a0
; FENCE-OPT-NEXT:    ld	a2, 0(a0)
; FENCE-OPT-NEXT:    lui	a1, %hi(temp)
; FENCE-OPT-NEXT:    sd	a2, %lo(temp)(a1)
; FENCE-OPT-NEXT:    ld	a2, 8(a0)
; FENCE-OPT-NEXT:    sd	a2, %lo(temp)(a1)
; FENCE-OPT-NEXT:    ld	a0, 16(a0)
; FENCE-OPT-NEXT:    sd	a0, %lo(temp)(a1)
; FENCE-OPT-NEXT:    addi	sp, sp, 16
; FENCE-OPT-NEXT:    ret

  %x.addr = alloca i64, align 8
  store i64 %x, ptr %x.addr, align 8
  %1 = load i64, ptr %x.addr, align 8
  %2 = load i32, ptr @array1_size, align 4
  %3 = load i64, ptr %x.addr, align 8
  %arrayidx = getelementptr inbounds [160 x i8], ptr @array1, i64 0, i64 %3
  %4 = load i64, ptr %arrayidx, align 8
  store i64 %4, ptr @temp, align 8
  %5 = add i64 %3, 8
  %arrayidx2 = getelementptr inbounds [160 x i8], ptr @array1, i64 0, i64 %5
  %6 = load i64, ptr %arrayidx2, align 8
  store i64 %6, ptr @temp, align 8
  %7 = add i64 %3, 16
  %arrayidx3 = getelementptr inbounds [160 x i8], ptr @array1, i64 0, i64 %7
  %8 = load i64, ptr %arrayidx3, align 8
  store i64 %8, ptr @temp, align 8

  ret void
}


