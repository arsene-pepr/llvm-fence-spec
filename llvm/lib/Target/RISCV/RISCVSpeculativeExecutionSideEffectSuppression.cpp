//===-- RISCVSpeculativeExecutionSideEffectSuppression.cpp ------------------===//
//
// Part of the LLVM Project, under the Apache License v2.0 with LLVM Exceptions.
// See https://llvm.org/LICENSE.txt for license information.
// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
//
//===----------------------------------------------------------------------===//
/// \file
///
/// This file contains the RISCV implementation of the speculative execution side
/// effect suppression mitigation.
///
/// This must be used with the -mlvi-cfi flag in order to mitigate indirect
/// branches and returns.
//===----------------------------------------------------------------------===//

#include "RISCV.h"
#include "RISCVInstrInfo.h"
#include "RISCVSubtarget.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/Statistic.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineDominators.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/Register.h"
#include "llvm/Pass.h"
using namespace llvm;

#define DEBUG_TYPE "riscv-fence-spec"

STATISTIC(NumFENCEsInserted, "Number of fence instructions inserted");

static cl::opt<bool> EnableSpeculativeExecutionSideEffectSuppression(
    "riscv-fence-spec-enable",
    cl::desc("Force enable speculative execution side effect suppression. "
             "Inserting fence.spec %rd, %rs1 creating a dependence on rs1."
             "%rd is not changed but can be used in order to create dependence "
             "on this fence.spec"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> LoadStore(
    "riscv-fence-spec-with-store",
    cl::desc("Insert fence.spec instructions after/before load AND store instructions"),
    cl::init(false), cl::Hidden); // Default is load only

static cl::opt<bool> AfterLoadStore(
    "riscv-fence-spec-after",
    cl::desc("Insert fence.spec instructions after load/store instructions "),
    cl::init(false), cl::Hidden); // Default is before
                                  //
static cl::opt<bool> DisableLoadAfterX0(
    "riscv-fence-spec-after-disable-x0",
    cl::desc("Disable inserting rd=x0 when using fence.spec instructions after load/store instructions "),
    cl::init(false), cl::Hidden); // Default is before

static cl::opt<bool> FENCESpecALL(
    "riscv-fence-spec-all",
    cl::desc("Insert fence.spec.all aliased to fence.spec x0, x0: "
             "this instruction depends on all previous instructions and all "
             "next instructions will depend on it."),
    cl::init(false), cl::Hidden);

static cl::opt<bool> FENCESpecSER(
    "riscv-fence-spec-ser",
    cl::desc("Insert fence.ser"
             "this instruction will serialize memory instructions"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> FENCEI(
    "riscv-fence-i",
    cl::desc("Insert fence.i"
             "this instruction should reschedule memory instructions"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> SLHFence(
    "riscv-fence-slh",
    cl::desc("Insert fence.ser naively regarding to previous branching condition"
             "this instruction will serialize memory instructions"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> FenceDepOpt(
    "fencedep_opt",
    cl::desc("Use def-use chain to remove some fencedep"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> FENCENop(
    "riscv-fence-spec-nop",
    cl::desc("Insert pseudo nop instruction instead"),
    cl::init(false), cl::Hidden);

static cl::opt<bool> OneFENCEPerBasicBlock(
    "riscv-fence-spec-one-fence-per-bb",
    cl::desc(
        "Omit all fences other than the first to be placed in a basic block."),
    cl::init(false), cl::Hidden);

static cl::opt<bool> DisableOptLoadSeq(
    "riscv-fence-spec-disopt",
    cl::desc(
        "Disable optimization that does not fence a reg that is already fenced in same basic block."),
    cl::init(false), cl::Hidden);

static cl::opt<bool> OnlyFENCENonConst(
    "riscv-fence-spec-only-fence-non-const",
    cl::desc("Only fence before groups of terminators where at least one "
             "branch instruction has an input to the addressing mode that is a "
             "register other than %rip."),
    cl::init(false), cl::Hidden);

static cl::opt<bool>
    OmitBranchFENCEs("riscv-fence-spec-omit-branch-fences",
                      cl::desc("Omit all fences before branch instructions."),
                      cl::init(false), cl::Hidden);

static cl::opt<bool>
    EnableFenceEndBlock("riscv-fence-spec-end-block",
                      cl::desc("Enable options that add fences before terminators."),
                      cl::init(false), cl::Hidden);

namespace {

class RISCVSpeculativeExecutionSideEffectSuppression
    : public MachineFunctionPass {
public:
  RISCVSpeculativeExecutionSideEffectSuppression() : MachineFunctionPass(ID) {}

  static char ID;
  StringRef getPassName() const override {
    return "RISCV Speculative Execution Side Effect Suppression";
  }

  bool runOnMachineFunction(MachineFunction &MF) override;
  void getAnalysisUsage(AnalysisUsage &AU) const override;
};
} // namespace

char RISCVSpeculativeExecutionSideEffectSuppression::ID = 0;

void RISCVSpeculativeExecutionSideEffectSuppression::getAnalysisUsage(
    AnalysisUsage &AU) const {
  AU.setPreservesCFG();
  AU.addRequired<MachineDominatorTree>();
  AU.addPreserved<MachineDominatorTree>();
  MachineFunctionPass::getAnalysisUsage(AU);
}

// This function returns whether the passed instruction uses a memory addressing
// mode that is constant. We treat all memory addressing modes that read
// from a register that is not %rip as non-constant. Note that the use
// of the EFLAGS register results in an addressing mode being considered
// non-constant, therefore all JCC instructions will return false from this
// function since one of their operands will always be the EFLAGS register.
static bool hasConstantAddressingMode(const MachineInstr &MI) {
  for (const MachineOperand &MO : MI.uses())
    if (MO.isReg())
      return false;
  return true;
}

const MachineInstr *getConditionalBranchInstr(MachineBasicBlock &MBB, MachineDominatorTree &MDT) {
  // Find the dominator of the current MBB
  /* const MachineBasicBlock *DomBlock = MDT.getNode(&MBB)->getIDom()->getBlock(); */
  if (MachineDomTreeNode *N = MDT.getNode(&MBB)) {
    if ((N = N->getIDom())){
      const MachineBasicBlock *DomBlock = N->getBlock();
      /* BA = DFG.findBlock(N->getBlock()); */
      // Iterate over the instructions in the dominator block
      for (const MachineInstr &MI : *DomBlock) {
        // Check if the instruction is a conditional branch
        if (MI.isConditionalBranch()) {
          return &MI;
        }
      }
    }
  }
  return nullptr;
}

static bool isFloatReg(MachineRegisterInfo &MRI, const Register Reg) {
    if(Reg.isVirtual()){
        const TargetRegisterClass *RC = MRI.getRegClass(Reg);
        if (RC->hasSuperClassEq(&RISCV::FPR16RegClass) ||
            RC->hasSuperClassEq(&RISCV::FPR32RegClass) ||
            RC->hasSuperClassEq(&RISCV::FPR64RegClass)) {
            return true;
        } 
    }
    return false;
}

bool isDependentOnProtectedRegInMBB(llvm::Register Reg,
                                    llvm::Register ProtectedReg,
                                    const llvm::MachineBasicBlock &MBB,
                                    const llvm::MachineRegisterInfo &MRI,
                                    llvm::SmallVector<llvm::Register, 8> &Visited) {
    // Base case: if we reach the ProtectedReg, return true
    if (Reg == ProtectedReg)
        return true;

    // If already visited this register, avoid rechecking
    for (const auto &VisitedReg : Visited) {
        if (VisitedReg == Reg)
            return false;
    }
    
    // Mark this register as visited
    Visited.push_back(Reg);

    // Traverse all instructions defining this register, only in MBB
    for (auto &DefInstr : MRI.def_instructions(Reg)) {
        if (DefInstr.getParent() != &MBB)
            continue; // Skip instructions not in the given block

        // Check operands of the defining instruction
        for (const auto &Op : DefInstr.operands()) {
            if (Op.isReg() && Op.isUse()) {
                llvm::Register DefReg = Op.getReg();
                // Recursively check if the operand depends on ProtectedReg in MBB
                if (isDependentOnProtectedRegInMBB(DefReg, ProtectedReg, MBB, MRI, Visited))
                    return true;
            }
        }
    }

    return false;
}

bool instructionDependsOnProtectedRegInMBB(const llvm::MachineInstr &MI,
                                           llvm::Register ProtectedReg,
                                           const llvm::MachineBasicBlock &MBB,
                                           const llvm::MachineRegisterInfo &MRI) {
    
    SmallVector<llvm::Register, 8> Visited;

    // Check each operand of MI within MBB
    for (const auto &Op : MI.operands()) {
        if (Op.isReg()) {
            llvm::Register Reg = Op.getReg();
            if (isDependentOnProtectedRegInMBB(Reg, ProtectedReg, MBB, MRI, Visited))
                return true;
        }
    }

    return false;
}

bool RISCVSpeculativeExecutionSideEffectSuppression::runOnMachineFunction(
    MachineFunction &MF) {

  /* const auto &OptLevel = MF.getTarget().getOptLevel(); */
  const RISCVSubtarget &Subtarget = MF.getSubtarget<RISCVSubtarget>();
  MachineRegisterInfo &MRI = MF.getRegInfo();

  MachineDominatorTree *MDT = &getAnalysis<MachineDominatorTree>();

  // Check whether SESES needs to run as the fallback for LVI at O0, whether the
  // user explicitly passed an SESES flag, or whether the SESES target feature
  // was set.
  if (!EnableSpeculativeExecutionSideEffectSuppression)
    return false;

  LLVM_DEBUG(dbgs() << "********** " << getPassName() << " : " << MF.getName()
                    << " **********\n");
  bool Modified = false;

  const RISCVInstrInfo *TII = Subtarget.getInstrInfo();

  // Choose which fence instruction to use
  auto FenceInst = RISCV::FENCE_SPEC;
  if (FENCESpecSER) {
    FenceInst = RISCV::FENCE_SER;
  } else if (FENCEI) {
    FenceInst = RISCV::FENCE_I;
  }

  for (MachineBasicBlock &MBB : MF) {
    MachineInstr *FirstTerminator = nullptr;
    // Keep track of whether the previous instruction was an FENCE to avoid
    // adding redundant FENCEs.
    bool PrevInstIsFENCE = false;
    Register ProtectedReg;
    bool IsProtected = false;
    bool firstProtected = false;
    for (auto &MI : MBB) {

      /* std::cout << "== New Inst :" << std::endl; */
      /* MI.dump(); */
      if (!AfterLoadStore && (MI.getOpcode() == RISCV::FENCE_SPEC 
          ||  MI.getOpcode() == RISCV::FENCE_SER)) {
        PrevInstIsFENCE = true;
        continue;
      }

      // We want to put an FENCE before any instruction that
      // may load or store. This FENCE is intended to avoid leaking any secret
      // data due to a given load or store. This results in closing the cache
      // and memory timing side channels. We will treat terminators that load
      // or store separately.
      if (!MI.isTerminator() &&
          // a load or, if enabled a Store
          (MI.mayLoad() || (LoadStore && MI.mayStore()))) {
        auto InsertPt = MI.getIterator();
        if(AfterLoadStore)
          ++InsertPt;
        if (!PrevInstIsFENCE || AfterLoadStore) {
          if(FENCENop){
            // This is nop inst in RISCV ↓
            BuildMI(MBB, InsertPt, DebugLoc(), TII->get(RISCV::ADDI),
                    RISCV::X0).addReg(RISCV::X0).addImm(0);
            NumFENCEsInserted++;
            Modified = true;
          } else if(FENCEI) {
            BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst));
            NumFENCEsInserted++;
            Modified = true;
          } else if(FENCESpecALL) {
            BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst), RISCV::X0)
              .addReg(RISCV::X0);
            NumFENCEsInserted++;
            Modified = true;
          } else if(SLHFence) {
            auto [DstReg, _DstTy, Src1Reg, _Src1Ty] = MI.getFirst2RegLLTs();
            if(!firstProtected) {
              if(FenceInst == llvm::RISCV::FENCE_SER){
                auto MIC = getConditionalBranchInstr(MBB,*MDT);
                if(MIC) {
                  // Extract the operands of the conditional branch instruction
                  for (const MachineOperand &MO : MIC->operands()) {
                    if (MO.isReg() && MO.getReg() != RISCV::X0) {
                      auto Reg = MO.getReg();
                      if(!(isFloatReg(MRI,Reg) || isFloatReg(MRI,Src1Reg))){
                        /* dbgs() << "Operand is a register: " << Reg << "\n"; */
                        BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst), Src1Reg)
                          .addReg(Reg);
                        firstProtected = true;
                        ProtectedReg = DstReg;
                        NumFENCEsInserted++;
                        Modified = true;
                      }
                    }
                  }
                } 
              } else {
                BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst),
                        Src1Reg) .addReg(Src1Reg);

                firstProtected = true;
                ProtectedReg = DstReg;
                NumFENCEsInserted++;
                Modified = true;
              }
            } else { //First fence was inserted then create dep with last
                     // ProtectedReg
                bool Protect = false;

                if(FenceDepOpt)
                  Protect = !instructionDependsOnProtectedRegInMBB(MI,ProtectedReg,MBB,MRI) && Src1Reg != ProtectedReg;
                else 
                  Protect = Src1Reg != ProtectedReg;

                if(Protect && 
                   !(isFloatReg(MRI,ProtectedReg) || isFloatReg(MRI,Src1Reg))) {
                    BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst),
                            Src1Reg) .addReg(ProtectedReg);
                    NumFENCEsInserted++;
                    Modified = true;
                }
            }
          } else {
            auto [DstReg, _DstTy, Src1Reg, _Src1Ty] = MI.getFirst2RegLLTs();
            if(AfterLoadStore && !(isFloatReg(MRI,DstReg))) { //Load/store after ↓
                if(FenceInst == RISCV::FENCE_SPEC && !DisableLoadAfterX0){
                    BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst),
                            RISCV::X0) .addReg(DstReg);
                    NumFENCEsInserted++;
                    Modified = true;
                } else {
                    BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst),
                            DstReg) .addReg(DstReg);
                    NumFENCEsInserted++;
                    Modified = true;
                }
            } else if(!(isFloatReg(MRI,Src1Reg))) { //Then load/store before ↓
              if(!IsProtected || (Src1Reg != ProtectedReg)) {
                BuildMI(MBB, InsertPt, DebugLoc(), TII->get(FenceInst),
                    Src1Reg) .addReg(Src1Reg);

                NumFENCEsInserted++;
                Modified = true;
              }
              if(!DisableOptLoadSeq) {
                ProtectedReg = Src1Reg;
                IsProtected = true;
                /* std::cout << "ProtectedReg SAVED:" << std::endl; */
                /* std::cout << ProtectedReg << std::endl; */
              }
            }
          }
        }
        if (OneFENCEPerBasicBlock)
          break;
      } else {
        // If already protected register is redefined, reset it
        // Then it can be fenced again
        // We can do better regarding to inst fence used FIXME !
        if(!DisableOptLoadSeq && IsProtected){
          // Search for the def operand …
          for (unsigned i = 0; i < MI.getNumOperands(); ++i) {
            const MachineOperand &MO = MI.getOperand(i);
            if (MO.isReg() && MO.isDef()) {
              auto DstReg = MO.getReg();
              // This is a destination operand
              if(DstReg == ProtectedReg){
                // if the already fenced reg is redefined 
                // reset the pointer
                IsProtected = false;
                // FIXME we can do better regarding to the fence used !
              }
              break;
            }
          }
        }
      }
      // The following section will be FENCEing before groups of terminators
      // that include branches. This will close the branch prediction side
      // channels since we will prevent code executing after misspeculation as
      // a result of the FENCEs placed with this logic.

      // Keep track of the first terminator in a basic block since if we need
      // to FENCE the terminators in this basic block we must add the
      // instruction before the first terminator in the basic block (as
      // opposed to before the terminator that indicates an FENCE is
      // required). An example of why this is necessary is that the
      // RISCVInstrInfo::analyzeBranch method assumes all terminators are grouped
      // together and terminates it's analysis once the first non-termintor
      // instruction is found.
      if (MI.isTerminator() && FirstTerminator == nullptr)
        FirstTerminator = &MI;

      // Look for branch instructions that will require an FENCE to be put
      // before this basic block's terminators.
      if (!MI.isBranch() || OmitBranchFENCEs) {
        // This isn't a branch or we're not putting FENCEs before branches.
        PrevInstIsFENCE = false;
        continue;
      }

      if (OnlyFENCENonConst && hasConstantAddressingMode(MI)) {
        // This is a branch, but it only has constant addressing mode and we're
        // not adding FENCEs before such branches.
        PrevInstIsFENCE = false;
        continue;
      }

      // This branch requires adding an FENCE. Do not protect return instr ?
      if (EnableFenceEndBlock && !PrevInstIsFENCE) {
        assert(FirstTerminator && "Unknown terminator instruction");
        if(FENCENop){
          // This is nop inst in RISCV ↓
          BuildMI(MBB, FirstTerminator, DebugLoc(), TII->get(RISCV::ADDI),
              RISCV::X0).addReg(RISCV::X0).addImm(0);
          NumFENCEsInserted++;
          Modified = true;
        } else if(FENCESpecALL) {
          BuildMI(MBB, FirstTerminator, DebugLoc(), TII->get(FenceInst),
              RISCV::X0).addReg(RISCV::X0);
          NumFENCEsInserted++;
          Modified = true;
        } else {
          auto [_DstReg, _DstTy, Src1Reg, _Src1Ty] = MI.getFirst2RegLLTs();
          if(!(isFloatReg(MRI,Src1Reg))){
              BuildMI(MBB, FirstTerminator, DebugLoc(), TII->get(FenceInst),
                      Src1Reg).addReg(Src1Reg);
              NumFENCEsInserted++;
              Modified = true;
          }
        }
      }
      break;
    }
  }

  return Modified;
}

FunctionPass *llvm::createRISCVSpeculativeExecutionSideEffectSuppression() {
  return new RISCVSpeculativeExecutionSideEffectSuppression();
}

INITIALIZE_PASS(RISCVSpeculativeExecutionSideEffectSuppression, "riscv-fence-spec",
                "RISCV Speculative Execution Side Effect Suppression", false,
                false)
