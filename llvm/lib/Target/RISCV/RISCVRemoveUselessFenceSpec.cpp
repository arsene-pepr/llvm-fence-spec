#include "RISCV.h"
#include "llvm/ADT/Statistic.h"
#include "MCTargetDesc/RISCVMCExpr.h"
#include "RISCVSubtarget.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/Pass.h"

using namespace llvm;

#define DEBUG_TYPE "riscv-remove-useless-fence"

STATISTIC(NumFENCEsRemoved, "Number of fence instructions REMOVED");

namespace {
class RISCVRemoveUselessFenceSpec : public MachineFunctionPass {
public:
    static char ID;
    RISCVRemoveUselessFenceSpec() : MachineFunctionPass(ID) {}

    bool runOnMachineFunction(MachineFunction &MF) override {
        bool Changed = false;

        for (auto &MBB : MF) {
            for (auto MI = MBB.begin(), ME = MBB.end(); MI != ME; ) {
                // Example check for a custom condition
                // Check if the instruction is a no-op or has an operand that is zero
                if (isUselessInstruction(*MI)) {
                    MI = MBB.erase(MI);
                    NumFENCEsRemoved++;
                    Changed = true;
                } else {
                    ++MI;
                }
            }
        }

        return Changed;
    }

private:
    bool isUselessInstruction(const MachineInstr &MI) {
        // Define your condition to identify useless instructions
        // Example: Check if any operand is an immediate zero
        if(MI.getOpcode() == RISCV::FENCE_COND) {
            
            /* dbgs() << "********** Remove Fence **********\n"; */
            /* MI.dump(); */
            // Check if all regs are X0 then we can remove the MI
            for (const MachineOperand &MO : MI.operands()) {
                /* MO.dump(); */
                if (!MO.isReg() || MO.getReg() != RISCV::X0) {
                    return false;
                }
                /* dbgs() << "********** Not to remove **********\n"; */
            }
            return true;
            /* const MachineOperand &MO = MI.getOperand(1); */
            /* if (MO.isReg() && MO.getReg() == RISCV::X0) { */
            /*     return true; */
            /* } */
        }
        return false;
    }
};

char RISCVRemoveUselessFenceSpec::ID = 0;
} // end anonymous namespace

// Register the pass
/* static RegisterPass<RISCVRemoveUselessFenceSpec> X("remove-useless-instr", "Remove Useless Instructions Pass", false, false); */

FunctionPass *llvm::createRISCVRemoveUselessFenceSpec() {
  return new RISCVRemoveUselessFenceSpec();
}

INITIALIZE_PASS(RISCVRemoveUselessFenceSpec, "riscv-remove-useless-fence",
                "RISCV Remove Useless Fence Spec instructions Pass", false,
                false)

