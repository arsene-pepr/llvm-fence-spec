; ModuleID = 'mytests/victim.c'
source_filename = "mytests/victim.c"
target datalayout = "e-m:e-p:64:64-i64:64-i128:128-n32:64-S128"
target triple = "riscv64-spec-unknown-elf"

@array1_size = dso_local global i32 16, align 4
@array1 = dso_local global <{ [16 x i8], [144 x i8] }> <{ [16 x i8] c"\01\02\03\04\05\06\07\08\09\0A\0B\0C\0D\0E\0F\10", [144 x i8] zeroinitializer }>, align 1
@.str = private unnamed_addr constant [41 x i8] c"The Magic Words are Squeamish Ossifrage.\00", align 1
@secret = dso_local global ptr @.str, align 8
@temp = dso_local global i8 0, align 1
@array2 = dso_local global [131072 x i8] zeroinitializer, align 1
@unused1 = dso_local global [64 x i8] zeroinitializer, align 1
@unused2 = dso_local global [64 x i8] zeroinitializer, align 1

; Function Attrs: noinline nounwind optnone
define dso_local void @victim_function(i64 noundef %x) #0 {
entry:
  %x.addr = alloca i64, align 8
  store i64 %x, ptr %x.addr, align 8
  %0 = load i64, ptr %x.addr, align 8
  %cmp = icmp ult i64 %0, 0
  br i1 %cmp, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %1 = load i64, ptr %x.addr, align 8
  %arrayidx = getelementptr inbounds [160 x i8], ptr @array1, i64 0, i64 %1
  %2 = load i8, ptr %arrayidx, align 1
  %conv = zext i8 %2 to i32
  %mul = mul nsw i32 %conv, 512
  %idxprom = sext i32 %mul to i64
  %arrayidx1 = getelementptr inbounds [131072 x i8], ptr @array2, i64 0, i64 %idxprom
  %3 = load i8, ptr %arrayidx1, align 1
  %conv2 = zext i8 %3 to i32
  %4 = load i8, ptr @temp, align 1
  %conv3 = zext i8 %4 to i32
  %and = and i32 %conv3, %conv2
  %conv4 = trunc i32 %and to i8
  store i8 %conv4, ptr @temp, align 1
  br label %if.end

if.end:                                           ; preds = %if.then, %entry
  ret void
}

attributes #0 = { noinline nounwind optnone "frame-pointer"="all" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="generic-rv64" "target-features"="+64bit,+a,+c,+m,+relax,-d,-e,-experimental-zacas,-experimental-zcmop,-experimental-zfbfmin,-experimental-zicfilp,-experimental-zicfiss,-experimental-zimop,-experimental-ztso,-experimental-zvfbfmin,-experimental-zvfbfwma,-f,-h,-smaia,-smepmp,-ssaia,-svinval,-svnapot,-svpbmt,-v,-xcvalu,-xcvbi,-xcvbitmanip,-xcvelw,-xcvmac,-xcvmem,-xcvsimd,-xsfvcp,-xsfvfnrclipxfqf,-xsfvfwmaccqqq,-xsfvqmaccdod,-xsfvqmaccqoq,-xtheadba,-xtheadbb,-xtheadbs,-xtheadcmo,-xtheadcondmov,-xtheadfmemidx,-xtheadmac,-xtheadmemidx,-xtheadmempair,-xtheadsync,-xtheadvdot,-xventanacondops,-za128rs,-za64rs,-zawrs,-zba,-zbb,-zbc,-zbkb,-zbkc,-zbkx,-zbs,-zca,-zcb,-zcd,-zce,-zcf,-zcmp,-zcmt,-zdinx,-zfa,-zfh,-zfhmin,-zfinx,-zhinx,-zhinxmin,-zic64b,-zicbom,-zicbop,-zicboz,-ziccamoa,-ziccif,-zicclsm,-ziccrse,-zicntr,-zicond,-zicsr,-zifencei,-zihintntl,-zihintpause,-zihpm,-zk,-zkn,-zknd,-zkne,-zknh,-zkr,-zks,-zksed,-zksh,-zkt,-zmmul,-zvbb,-zvbc,-zve32f,-zve32x,-zve64d,-zve64f,-zve64x,-zvfh,-zvfhmin,-zvkb,-zvkg,-zvkn,-zvknc,-zvkned,-zvkng,-zvknha,-zvknhb,-zvks,-zvksc,-zvksed,-zvksg,-zvksh,-zvkt,-zvl1024b,-zvl128b,-zvl16384b,-zvl2048b,-zvl256b,-zvl32768b,-zvl32b,-zvl4096b,-zvl512b,-zvl64b,-zvl65536b,-zvl8192b" }

!llvm.module.flags = !{!0, !1, !2, !3}
!llvm.ident = !{!4}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 1, !"target-abi", !"lp64"}
!2 = !{i32 7, !"frame-pointer", i32 2}
!3 = !{i32 8, !"SmallDataLimit", i32 8}
!4 = !{!"clang version 18.1.0rc (git@gitlab.inria.fr:arsene-pepr/llvm-fence-spec.git 1a86f2fdb3d70fea8a22cd13f5f0b53fae73125e)"}
