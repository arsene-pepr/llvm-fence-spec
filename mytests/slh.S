	.text
	.attribute	4, 16
	.attribute	5, "rv64i2p1"
	.file	"speculative-load-hardening-moein.ll"
	.globl	victim_function                 # -- Begin function victim_function
	.p2align	2
	.type	victim_function,@function
victim_function:                        # @victim_function
	.cfi_startproc
# %bb.0:                                # %entry
	addi	sp, sp, -16
	.cfi_def_cfa_offset 16
	lui	a1, %hi(array1_size)
	lwu	a1, %lo(array1_size)(a1)
	sd	a0, 8(sp)
	bgeu	a0, a1, .LBB0_2
# %bb.1:                                # %if.then
	ld	a0, 8(sp)
	lui	a1, %hi(array1)
	addi	a1, a1, %lo(array1)
	add	a0, a1, a0
	lbu	a0, 0(a0)
	slli	a0, a0, 9
	lui	a1, %hi(array2)
	addi	a1, a1, %lo(array2)
	add	a0, a1, a0
	lbu	a0, 0(a0)
	lui	a1, %hi(temp)
	lbu	a2, %lo(temp)(a1)
	and	a0, a2, a0
	sb	a0, %lo(temp)(a1)
.LBB0_2:                                # %if.end
	addi	sp, sp, 16
	ret
.Lfunc_end0:
	.size	victim_function, .Lfunc_end0-victim_function
	.cfi_endproc
                                        # -- End function
	.type	array1_size,@object             # @array1_size
	.section	.sdata,"aw",@progbits
	.globl	array1_size
	.p2align	2, 0x0
array1_size:
	.word	16                              # 0x10
	.size	array1_size, 4

	.type	array1,@object                  # @array1
	.data
	.globl	array1
array1:
	.ascii	"\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020"
	.zero	144
	.size	array1, 160

	.type	temp,@object                    # @temp
	.section	.sbss,"aw",@nobits
	.globl	temp
temp:
	.byte	0                               # 0x0
	.size	temp, 1

	.type	array2,@object                  # @array2
	.bss
	.globl	array2
array2:
	.zero	131072
	.size	array2, 131072

	.section	".note.GNU-stack","",@progbits
