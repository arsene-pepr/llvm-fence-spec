# The LLVM Compiler Infrastructure

This is a forked LLVM with various security features targeting RISC-V.
The initial SLH implementation is from Moein Ghaniyoun: [SLH RISCV LLVM](https://github.com/MoeinGhaniyoun/LLVM-SLH-RISCV).

# Build and Install for RISCV
[![OpenSSF Scorecard](https://api.securityscorecards.dev/projects/github.com/llvm/llvm-project/badge)](https://securityscorecards.dev/viewer/?uri=github.com/llvm/llvm-project)
[![OpenSSF Best Practices](https://www.bestpractices.dev/projects/8273/badge)](https://www.bestpractices.dev/projects/8273)
[![libc++](https://github.com/llvm/llvm-project/actions/workflows/libcxx-build-and-test.yaml/badge.svg?branch=main&event=schedule)](https://github.com/llvm/llvm-project/actions/workflows/libcxx-build-and-test.yaml?query=event%3Aschedule)

Will build and install LLVM bin in `../_install`

```bash
mkdir _build
cd _build
cmake -G Ninja -DCMAKE_BUILD_TYPE="Release" \
  -DBUILD_SHARED_LIBS=True -DLLVM_USE_SPLIT_DWARF=True \
  -DCMAKE_INSTALL_PREFIX="../../_install" \
  -DLLVM_OPTIMIZED_TABLEGEN=True -DLLVM_BUILD_TESTS=False \
  -DDEFAULT_SYSROOT="../../_install/riscv64-spec-elf" \
  -DLLVM_DEFAULT_TARGET_TRIPLE="riscv64-spec-elf" \
  -DLLVM_TARGETS_TO_BUILD="RISCV" \
  ../llvm
cmake --build . --target install
```

# Compile stuff with the fence-pass

```bash
clang -emit-llvm -c hello.c
llc -mtriple=riscv64 -riscv-fence-spec-enable (-riscv-fence-spec-all) hello.bc
```
or directly from clang flags with `-mllvm`

```bash
clang -mllvm --riscv-fence-spec-enable hello.c
```

Coupled to `-riscv-fence-spec-enable` you can add more options to the
fence insertion pass (*don't forget* to add `-mllvm` before each when
calling from `clang`):

1. `--riscv-fence-spec-with-store` will insert fences also before/after
   `store`s instructions. By default it inserts only before `load`.
2. `--riscv-fence-spec-after` will insert fences AFTER `load/store`
   instructions
3. `--riscv-fence-spec-all` will insert only full `fence.spec x0 x0`
   creating dependences to all instructions backward and forward.
4. `--riscv-fence-spec-ser` will insert only `fence.ser` for
   serialization 
5. `--riscv-fence-spec-one-fence-per-bb` will insert only the first
   fence of a basic-block
6. `--riscv-fence-spec-omit-branch-fences` will not insert fence before
   branching terminator instruction.

# Speculative Load Hardening from Moein Ghaniyoun

This LLVM took Ghaniyoun SLH RISCV pass and insert new `fence.spec`
instructions instead of `fence.i`.

This will poison load value/pointer regarding to predicate telling us
if we are in speculative execution or not.

You can enable it with :

```bash 
clang -mspeculative-load-hardening hello.c
```

You have the following options for this pass:

1. `-riscv-slh-fence` insert `fence.i` at beginning of blocks after
   conditional branches rather than using
   conditional_movs/non-speculative_loads/fake-branch-dependence
2. `-riscv-slh-fence-spec` same as `-fence` but using `fence.spec x0
   x0`
3. `-riscv-slh-fence-ser` same as `-fence` but using `fence.ser`
4. `-riscv-slh-fence-call-and-ret` use `fence.i` to harden both call
   and ret edges rather than a lighter weight mitigation.
5. `-riscv-slh-fence-spec-call-and-ret` same as above but using
   `fence.spec x0 x0`
5. `-riscv-slh-fence-ser-call-and-ret` same as above but using
   `fence.ser`
6. `-riscv-slh-ip` harden interprocedurally by passing the predicate
   in and out of functions in the high bits of the stack pointer.
7. `-riscv-slh-loads` Sanitize loads from memory. When disable, no
   significant security is provided.
7. `-riscv-slh-indirect` harden indirect calls and jumps against using
   speculatively stored attacker controlled addresses. This is
   designed to mitigate Spectre v1.2 style attacks.

# Some links

Hardening in LLVM :
https://llvm.org/docs/SpeculativeLoadHardening.html

LVI :
http://www.cs.ucr.edu/~lesani/companion/oopsla15/OOPSLA15.pdf

Build RISCV LLVM :
https://github.com/sifive/riscv-llvm/blob/dev/README.md
